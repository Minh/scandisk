#include "fs_disk.h"
#include "jfs_common.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <libgen.h>

/* Remove file from jfs */
void rm(jfs_t *jfs, char *filename);
int next_inode(jfs_t *jfs, int cur_inode, char *next_path, int type);
int bytes_to_blocks(int bytes);
void remove_file(jfs_t *jfs, int file_inodenum, char *file);
int findfolder(jfs_t *jfs, char *dir, int inodenum);
void usage();
