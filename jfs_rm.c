#include "jfs_rm.h"

/* Calculate the number of block a file requires */
int bytes_to_blocks(int bytes) {
  if (bytes <= BLOCKSIZE)
    return 1;
  else {
    if (bytes % BLOCKSIZE == 0)
      /* Exactly */
      return bytes/BLOCKSIZE;
    else
      /* Add an extra block for the remaining bytes */
      return (bytes/BLOCKSIZE) + 1;
  }
}

/* Find next inode */
int next_inode(jfs_t *jfs, int cur_inode, char *next_path, int type) {
  struct inode i_node;
  int dir_size, bytes_done=0;
  char block[BLOCKSIZE];

  get_inode(jfs, cur_inode, &i_node);
  dir_size = i_node.size;

  /* Read in the first block */
  jfs_read_block(jfs, block, i_node.blockptrs[0]);
  /* Type cast to access information of the block */
  struct dirent *dir_entry = (struct dirent*)block;

  while(1) {
    /* Directory or File name */
    char filename[MAX_FILENAME_LEN + 1];
    memcpy(filename, dir_entry->name, dir_entry->namelen);
    filename[dir_entry->namelen] = '\0';

    /* Check if the object and the next destination are the same */
    if (strcmp(filename, next_path) == 0 && dir_entry->file_type == type) {
      /*
      printf("Next inode filename: %s\n",filename);
      printf("Next filename inode: %i\n", dir_entry->inode);
      */
      return dir_entry->inode;
    }

    /* Add bytes that have been processed */
    bytes_done += dir_entry->entry_len;
    /* Go to the next entry */
    dir_entry = (struct dirent*)(block + bytes_done);

    /* Check when byte is done*/
    if (bytes_done >= dir_size) {
      break;
    }
  }

  return -1;
}

void remove_directory(jfs_t *jfs, int dir_inodenum, int file_inodenum,char *file) {

  /* file size in bytes */
  int dir_size, bytes_done=0, offset;
  struct dirent *dir_entry;
  struct inode d_node;
  struct inode *dir_inode;
  char block[BLOCKSIZE];
  char inodeblock[BLOCKSIZE];

  get_inode(jfs, dir_inodenum, &d_node);
  dir_size = d_node.size;

  jfs_read_block(jfs, inodeblock, inode_to_block(dir_inodenum));
  offset = (dir_inodenum % INODES_PER_BLOCK) * INODE_SIZE;
  dir_inode = (struct inode*)(inodeblock+offset);
  /*
  printf("Inode size: %i\n", dir_inode->size);
  printf("Inode flag: %i\n", dir_inode->flags);
  */
  jfs_read_block(jfs, block, d_node.blockptrs[0]);
  dir_entry = (struct dirent*)block;

  //printf("Directory size: %i\n", dir_size);

  while(1) {
    char filename[MAX_FILENAME_LEN + 1];
    memcpy(filename, dir_entry->name, dir_entry->namelen);
    filename[dir_entry->namelen] = '\0';
    /*
    printf("File name: %s\n", filename);
    printf("Directry entry length: %i\n", dir_entry->entry_len);
    */
    if (strcmp(filename, file) == 0 && dir_entry->file_type == DT_FILE) {
      /* Found the file's directory entry */
      /*
      printf("Found: %s\n", filename);
      printf("File %s entry length: %i\n", filename, dir_entry->entry_len);

      printf("Inode size: %i\n", dir_inode->size);
      printf("Inode size going to be subtracted: %i\n", dir_entry->entry_len);
      */
      /* Decrement size of the actually file entry */
      dir_inode->size -= dir_entry->entry_len;

      //printf("Remaining inode size: %i\n", dir_inode->size);

      /* Push the file in the block out */
      char new_block[BLOCKSIZE];
      memcpy(new_block, block, bytes_done);
      int bytes_remaining = BLOCKSIZE - bytes_done - dir_entry->entry_len;
      memcpy((new_block + bytes_done), (block + bytes_done + dir_entry->entry_len), bytes_remaining);

      jfs_write_block(jfs, inodeblock, inode_to_block(dir_inodenum));
      jfs_write_block(jfs, new_block, d_node.blockptrs[0]);
      jfs_commit(jfs);
      return;
    }

    bytes_done += dir_entry->entry_len;
    dir_entry = (struct dirent*)(block + bytes_done);

    if (bytes_done >= dir_size)
      break;
  }
}

/* Remove file from disk */
void remove_file(jfs_t *jfs, int inodenum, char *file) {
  /* file size in bytes */
  int blocknum, blocks;
  struct inode i_node;

  /* Read file's meta data from inode */
  get_inode(jfs, inodenum, &i_node);
  /* Check if it is a file */
  if (i_node.flags == FLAG_DIR) {
    /* It is not a file, abort deletion */
    //printf("Failed file check.\n");
    return;
  }

  /*
  printf("File inode: %i\n", inodenum);
  printf("File size: %i\n", file_inode->size);
  */

  /*
  Calculate the required number of blocks
  for this specific file size (in bytes)
  */
  blocks = bytes_to_blocks(i_node.size);
  //printf("Blocks: %i\n", blocks);
  i_node.size = 0;
  for (blocknum=0; blocknum<blocks; blocknum++) {
    /* Free block and return it to freelist */
    return_block_to_freelist(jfs, i_node.blockptrs[blocknum]);
    i_node.blockptrs[blocknum] = -1;
  }
  /* Free inode and return it to freelist */
  return_inode_to_freelist(jfs, inodenum);
  /* Free blocks in directory that is storing the file */
  //printf("Committing.\n");
  jfs_commit(jfs);
}

/* Find the inode of the folder */
int findfolder(jfs_t *jfs, char *dir, int inodenum) {
  int inode = inodenum;
  char *token, *tmp;
  tmp = (char*)malloc(sizeof(char)*strlen(dir) + 1);
  memcpy(tmp, dir, strlen(dir));
  tmp[strlen(dir)] = '\0';

  token = strtok(tmp, "/");
  //printf("Looking for path: %s\n", tmp);
  while(token != NULL) {
    //printf("Next token: %s\n", token);
    //printf("Current inode: %i\n", inode);
    inode = next_inode(jfs, inode, token, DT_DIRECTORY);
    //printf("New inode: %i\n", inode);

    token = strtok(NULL, "/");
    //printf("Next folder: %s\n", token);
  }

  return inode;
}

/* Remove file */
void rm(jfs_t *jfs, char *filename) {
  char *file, *basec, *path, *tmp;
  tmp = (char*)malloc(sizeof(char)*strlen(filename));
  memcpy(tmp, filename, strlen(filename));
  int root_inodenum, file_inodenum, dir_inodenum;
  /* Get filename without its absolute path */

  basec = strdup(tmp);
  /* Filename without its path */
  file = basename(basec);

  //printf("File name: %s\n", file);
  //printf("File name length: %i\n", strlen(file));

  path = (char*)malloc(sizeof(char)*(strlen(tmp) - strlen(file)));
  memcpy(path, filename, (strlen(filename) - strlen(file)));

  //printf("Path: %s\n", path);

  free(tmp);

  /* Start at root directory, find root directory */
  root_inodenum = find_root_directory(jfs);
  //printf("Root inodenum: %i\n", root_inodenum);
  /* Find the inodenum of the file to delete */
  file_inodenum = findfile_recursive(jfs, filename, root_inodenum, DT_FILE);
  //printf("File inodenum: %i\n", file_inodenum);
  if (file_inodenum == -1) {
    //printf("File %s does not exist.\n", filename);
    return;
  }
  /* Delete the file name of the file and find the directory inodenum */
  dir_inodenum = findfolder(jfs, path, root_inodenum);
  //printf("Directory inodenum: %i\n", dir_inodenum);
  if (dir_inodenum == -1) {
    //printf("Directory %s does not exist.\n", path);
    return;
  }

  /* Remove record from directory */
  remove_directory(jfs, dir_inodenum, file_inodenum, file);

  /* Remove file */
  remove_file(jfs, file_inodenum, file);
}

/* Show how the function is used, what kind of parameter it accepts */
void usage() {
  fprintf(stderr, "Usage: jfs_rm <volumename> <filename>\n");
  exit(1);
}

int main(int argc, char **argv)
{
  struct disk_image *di;
  jfs_t *jfs;

  if (argc != 3) {
    usage();
  }

  /* Mount disk to be able to access the disk */
  di = mount_disk_image(argv[1]);
  /* Initialize jfs */
  jfs = init_jfs(di);

  /* Delete file from disk */
  rm(jfs, argv[2]);

  /* unmount disk */
  unmount_disk_image(di);

  exit(0);
}
