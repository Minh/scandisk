#include "jfs_checklog.h"

/* Calculate the number of block a file requires */
int bytes_to_blocks(int bytes) {
  if (bytes <= BLOCKSIZE)
    return 1;
  else {
    if (bytes % BLOCKSIZE == 0)
      /* Exactly */
      return bytes/BLOCKSIZE;
    else
      /* Add an extra block for the remaining bytes */
      return (bytes/BLOCKSIZE) + 1;
  }
}

/* Process block */
void process_block(jfs_t *jfs, int blocknum) {
  int i, allocated_blocknum = -1;
  struct commit_block *cb;
  struct cached_block *c_block;
  char block[BLOCKSIZE];
  // Read block from log file
  //printf("Read from blocknum: %i\n", blocknum);
  jfs_read_block(jfs, block, blocknum);

  // Read them as commit blocks
  cb = (struct commit_block*)block;

  /* Check if it is a commit block */
  if (cb->magicnum != 0x89abcdef) {
    //printf("Block %i is not a commit block.\n", blocknum);
    return;
  } else {
    printf("Commit block.\n");
    /* Allocated blocknum for the file is here */
  }

  /* Check if it has been committed to the disk successfully */
  if (cb->uncommitted == 0) {
    printf("Has been committed.\n");
    //return;
  } else if (cb->uncommitted == 1) {
    printf("Has not been committed.\n");
  }
  /*
  for (i=0; i<INODE_BLOCK_PTRS; i++) {
    if (cb->blocknums[i] == -1)
      break;
    printf("Allocated block: %i\n", cb->blocknums[i]);
  }
  */
  /*
  printf("Block data: %s\n", block);
  printf("Size of the INODE_BLOCKPTRS: %i\n", INODE_BLOCK_PTRS);
  for (i=1; i<INODE_BLOCK_PTRS; i++) {
    allocated_blocknum = cb->blocknums[i];

    if (allocated_blocknum == -1)
      break;
    printf("Allocated block: %i\n", allocated_blocknum);
  }
  */
}

void erase_commit_block(jfs_t *jfs, int log_inodenum) {
  int i, blocks;
  struct inode log_inode;
  struct commit_block *cb;

  get_inode(jfs, log_inodenum, &log_inode);

  blocks = bytes_to_blocks(log_inode.size);

  for (i=0; i<INODE_BLOCK_PTRS; i++) {
    /* Find commit block */
    char block[BLOCKSIZE];
    jfs_read_block(jfs, block, log_inode.blockptrs[i]);
    cb = (struct commit_block*)block;
    if (cb->magicnum == 0x89abcdef) {
      cb->uncommitted = 0;
      cb->sum = 0;
      int blocknum;
      for (blocknum=0; blocknum<INODE_BLOCK_PTRS; blocknum++)
        cb->blocknums[blocknum] = -1;
      jfs_write_block(jfs, block, log_inode.blockptrs[i]);
      return;
    }
  }
}

/* Return 1 to say that it is already committed and return 0 to say that it has not been committed */
int verify_uncommitted_block(jfs_t *jfs, int blocknum) {
  char block[BLOCKSIZE];
  struct commit_block *cb;

  jfs_read_block(jfs, block, blocknum);

  cb = (struct commit_block*)block;

  if(cb->magicnum == 0x89abcdef) {
    if (cb->uncommitted == 1)
      return 0;
    else
      return 1;
  }
  return -1;
}

/* Check the log */
void checklog(jfs_t *jfs) {
  int root_inodenum, log_inodenum, log_size, blocknum, blocks;
  int i=0;
  struct inode log_inode;
  root_inodenum = find_root_directory(jfs);
  log_inodenum = findfile_recursive(jfs, ".log", root_inodenum, DT_FILE);

  get_inode(jfs, log_inodenum, &log_inode);

  /* Blocks required */
  blocks = bytes_to_blocks(log_inode.size);
  //printf("Number of blocks: %i\n", blocks);
  for (blocknum=0; blocknum<blocks; blocknum++) {
    printf("Log %i: %i\n", blocknum, log_inode.blockptrs[blocknum]);
    int block_type = verify_uncommitted_block(jfs, log_inode.blockptrs[blocknum]);
    if (block_type == 0) {
      printf("Block %i is a commit block\n", log_inode.blockptrs[blocknum]);
      /* From the ith to the next uncommitted block */
      char commitBlock[BLOCKSIZE];
      struct commit_block *cb;
      jfs_read_block(jfs, commitBlock, log_inode.blockptrs[blocknum]);

      cb = (struct commit_block*)commitBlock;

      int offset = i;
      int num = 0;
      while(i < blocknum) {
        /* Read from log file */
        char block[BLOCKSIZE];
        printf("Reading log file block %i into location block %i\n", log_inode.blockptrs[i], cb->blocknums[num]);
        jfs_read_block(jfs, block, log_inode.blockptrs[i]);
        jfs_write_block(jfs, block, cb->blocknums[num]);
        i++;
        num++;
      }
      //erase_commit_block(jfs, log_inode.blockptrs[blocknum]);
      /* Erase commit block */
      cb->uncommitted = 0;
      cb->sum = 0;
      int a;
      for (a=0; a<INODE_BLOCK_PTRS; a++)
        cb->blocknums[a] = -1;
      jfs_write_block(jfs, commitBlock, log_inode.blockptrs[blocknum]);
      jfs_commit(jfs);
    } else if (block_type == 1) {
      i = blocknum + 1;
    } else {
      /*Returned -1*/
    }
    //process_block(jfs, log_inode.blockptrs[blocknum]);
  }
  /* Erase commit block */
  //erase_commit_block(jfs, log_inodenum);

}

void usage() {
  fprintf(stderr, "Usage:  jfs_checklog <volumename>\n");
  exit(1);
}

int main(int argc, char **argv) {
  struct disk_image *di;
  jfs_t *jfs;

  if (argc != 2) {
    usage();
  }

  /* mount the disk image file so we can access it*/
  di = mount_disk_image(argv[1]);

  jfs = init_jfs(di);

  checklog(jfs);

  unmount_disk_image(di);

  exit(0);
}
